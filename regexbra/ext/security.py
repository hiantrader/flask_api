from werkzeug.security import safe_str_cmp
from regexbra.models.usermodel import UserModel

# users = [User(1, "bob", "asdf")]

# username_map = {x.username: x for x in users}
# userid_map = {x.id: x for x in users}


def authenticate(username, password):
    user = UserModel.find_user_by_name(username)
    if user and safe_str_cmp(user.password, password):
        return user
    
def identity(payload):
    user_id = payload["identity"]
    return UserModel.find_user_by_id(user_id)



