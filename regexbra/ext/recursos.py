from flask import request
from flask_restful import Resource, reqparse
from flask_jwt import jwt_required
from regexbra.db import db
from regexbra.models.usermodel import UserModel
from regexbra.models.itemmodel import ItemsModel
from regexbra.schema import user_schema

items: list = []

class UserRegister(Resource):
    parse = reqparse.RequestParser()
    parse.add_argument(
        "username", type=str, required=True, help="the field cannot be left blank!"
    )
    parse.add_argument(
        "password", type=str, required=True, help="the field cannot be left blank!"
    )

    def post(self):
        data=UserRegister.parse.parse_args()
        user=UserModel(**data)
        user.save_to_db()
        return user_schema.dump(user),201


