import os

from flask import Flask
from flask_jwt import JWT
from flask_migrate import Migrate
from flask_restful import Api

from regexbra.db import db
from regexbra.ext.recursos import UserRegister
from regexbra.ext.security import authenticate, identity
from regexbra.models import OrderModel
from regexbra.recursos.item_recurso import Item, ItemsList
from regexbra.recursos.store_recurso import StoreResource
from .recursos.item_order_recurso import ItemCartRecurso,CartRecurso
from flask_cors import CORS
 

def make_shell_context():
    return {'db': db,'order':OrderModel}

def create_app():
    app = Flask(__name__)
    # /auth
    app.secret_key = "secret"
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///teste.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["SQLALCHEMY_ECHO"] = True
    jwt = JWT(app, authenticate, identity)
    api = Api(app)
    db.init_app(app)
    Migrate(app, db)
    api.add_resource(Item, "/item/<name>")
    api.add_resource(ItemsList, "/items")
    api.add_resource(UserRegister, "/register")
    api.add_resource(StoreResource, "/store/<name>")
    # cart
    api.add_resource(ItemCartRecurso, "/item_cart/<int:id>",'/item_cart')
    api.add_resource(CartRecurso, "/cart")

    app.shell_context_processor(make_shell_context)
    CORS(app)
    return app
