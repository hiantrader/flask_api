from marshmallow_sqlalchemy import ModelSchema
from marshmallow_sqlalchemy.fields import Nested

from regexbra.db import db
from regexbra.models import (
    ItemOrderModel, ItemsModel, OrderModel, StoreModel, UserModel)

class UserSchema(ModelSchema):
    class Meta():
        model=UserModel

user_schema=UserSchema()


class StoreSchema(ModelSchema):

    class Meta():
        model=StoreModel

store_schema=StoreSchema()
stores_schema=StoreSchema(many=True)


class OrderSchema(ModelSchema):

    class Meta():
        model=OrderModel

order_schema=OrderSchema()
orders_schema=OrderSchema(many=True)

class ItemsSchema(ModelSchema):

    class Meta():
        model=ItemsModel
    store=Nested(StoreSchema)
    sqla_session=db.session
item_schema=ItemsSchema()
items_schema=ItemsSchema(many=True)

class ItemOrderSchema(ModelSchema):

    class Meta():
        model=ItemOrderModel
    item=Nested(ItemsSchema)
    sqla_session=db.session

item_order_schema=ItemOrderSchema()
items_orders_schema=ItemOrderSchema(many=True)





