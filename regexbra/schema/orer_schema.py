from flask_marshmallow.sqla import ModelSchema

from regexbra import OrderModel


class OrderSchema(ModelSchema):
    id=
    class Meta():
        model=OrderModel

order_schema=OrderSchema()
orders_schema=OrderSchema(many=True)