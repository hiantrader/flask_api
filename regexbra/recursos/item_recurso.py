from flask_restful import Resource, reqparse
from regexbra.models.itemmodel import ItemsModel
from regexbra.schema import item_schema

class Item(Resource):

    parse = reqparse.RequestParser()
    parse.add_argument(
        "price", type=float, required=True, help="the field cannot be left blank!"
    )
    parse.add_argument(
        "store_id", type=int, required=True, help="the field cannot be left blank!"
    )

    # @jwt_required()
    def get(self, name):
        item = ItemsModel.find_user_by_name(name)
        
        if item is not None:
            return dict(item=item.dump())
        return {"item": None}, 404

    def post(self, name):
        item = ItemsModel.find_user_by_name(name)
        if item is not None:
            return {"message": f"An item wuth name {item['name']} alredy existis"}, 400

        data = Item.parse.parse_args()
        item = ItemsModel(name=name, **data)
        item.save_to_db()
        return item_schema.dump(item), 201

    def delete(self, name):
        item = ItemsModel.find_user_by_name(name)
        if item:
            item.delete_to_db()
        return dict(message="item delected sucefully")

    def put(self, name):
        item = ItemsModel.find_user_by_name(name)
        data = Item.parse.parse_args()
        if item:
            item.price = data["price"]
            item.update_to_db()
        else:
            item = item = ItemsModel(name=name, **data)
            item.save_to_db()

        return item.dump(), 201


class ItemsList(Resource):
    def get(self):
        return ItemsModel.dump_all()
