from flask_restful import Resource, reqparse

from regexbra.models.item_order_model import ItemOrderModel
from regexbra.models import ItemOrderModel, ItemsModel, OrderModel
from regexbra.schema import item_order_schema, items_orders_schema


class ItemCartRecurso(Resource):

    parse = reqparse.RequestParser()
    parse.add_argument(
        "item_id", type=float, required=True, help="the field cannot be left blank!"
    )
    parse.add_argument("price", type=float)
    parse.add_argument("amount", type=int)
    parse.add_argument("order_id", type=int, required=True)

    # @jwt_required()
    def get(self, id):
        item_order = ItemOrderModel.get_by_id(id)

        if item_order is not None:
            return dict(item=item_order_schema.dump(item_order))
        return {"item_order": None}, 404

    def post(self):

        data = ItemCartRecurso.parse.parse_args()
        item_order = ItemOrderModel.create(**data)

        return item_order_schema.dump(item_order), 201

    def delete(self, id):
        i_order = ItemOrderModel.get_by_id(id)
        if i_order:
            i_order.delete()
        return dict(message="item delected sucefully")

    # def put(self, id):
    #     item = ItemOrderModel.get_by_id(id)
    #     data = ItemCartRecurso.parse.parse_args()
    #     if item:
    #         item.price = data["price"]
    #         item.update_to_db()
    #     else:
    #         item = item = ItemsModel(name=name, **data)
    #         item.save_to_db()

    #     return item.dump(), 201


class CartRecurso(Resource):
    def get(self):
        return items_orders_schema.dump(ItemOrderModel.query.all())
