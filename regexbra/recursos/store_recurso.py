from flask_restful import Resource, reqparse

from regexbra.models.storemodel import StoreModel
from regexbra.schema import store_schema, stores_schema


class StoreResource(Resource):

    # @jwt_required()
    def get(self, name):
        store = StoreModel.find_store_by_name(name)

        if store is not None:
            return dict(store=store.dump())
        return {"store": None}, 404

    def post(self, name):
        store = StoreModel.find_store_by_name(name)
        if store is not None:
            return {"message": f"An store wuth name {store.name} alredy existis"}, 400

        store = StoreModel(name=name)
        store.save_to_db()
        return store_schema.dump(store), 201

    def delete(self, name):
        store = StoreModel.find_store_by_name(name)
        if store:
            store.delete_to_db()
        return dict(message="store delected sucefully")

    # def put(self, name):
    #     store = storesModel.find_user_by_name(name)
    #     data = store.parse.parse_args()
    #     if store:
    #         store.price = data["price"]
    #         store.update_to_db()
    #     else:
    #         store = store = storesModel(name=name, **data)
    #         store.save_to_db()

    #     return store.dump(), 201


class StoresList(Resource):
    def get(self):
        return storesModel.dump_all()
