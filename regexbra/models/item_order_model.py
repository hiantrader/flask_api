from sqlalchemy.orm import relationship
from .mixinmodel import CRUDMixin
from regexbra.db import db
from .itemmodel import ItemsModel
from .order_model import OrderModel

class ItemOrderModel(CRUDMixin,db.Model):
    
    price=db.Column(db.Float)
    amount=db.Column(db.Integer,default=1)
    item_id = db.Column(db.Integer,db.ForeignKey('items.id') )
    order_id = db.Column(db.Integer,db.ForeignKey('orders.id') )
    item=db.relationship('ItemsModel')
    order=db.relationship('OrderModel')
