
from regexbra.db import db
from .mixinmodel import CrudMixin

class StoreModel(CrudMixin,db.Model):
    __tablename__ = "store"
    id:int = db.Column(db.Integer, primary_key=True)
    name:str = db.Column(db.String(80))

    @classmethod
    def find_store_by_name(cls,name):
        return cls.query.filter_by(name=name).first()

    
