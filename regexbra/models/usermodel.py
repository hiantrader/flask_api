from regexbra.db import db
from .mixinmodel import CrudMixin

class UserModel(CrudMixin,db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))

    @classmethod
    def find_user_by_name(cls, name):
        return cls.query.filter_by(username=name).first()

    @classmethod
    def find_user_by_id(cls, id_):
        return cls.query.filter_by(id=id_).first()


