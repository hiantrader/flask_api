from regexbra.db import db
from regexbra.models.storemodel import StoreModel
from .mixinmodel import CrudMixin

class ItemsModel(CrudMixin,db.Model):
    __tablename__ = "items"
    id:int = db.Column(db.Integer, primary_key=True)
    name:str = db.Column(db.String(80))
    price:float = db.Column(db.Float(10))
    store_id:int=db.Column(db.Integer, db.ForeignKey('store.id'))
    store:StoreModel=db.relationship('StoreModel')

    @classmethod
    def find_user_by_name(cls, name):
        return cls.query.filter_by(name=name).first()



