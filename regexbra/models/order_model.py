from regexbra.db import db
from .mixinmodel import CRUDMixin
from .usermodel import UserModel


class OrderModel(CRUDMixin,db.Model):
    __tablename__='orders'
    billing_address:str=db.Column(db.String(80))
    shipping_address:str=db.Column(db.String(80))
    shipping_price:float =db.Column(db.Float)
    user_id=db.Column(db.Integer,db.ForeignKey('users.id'))
    user=db.relationship('UserModel')
